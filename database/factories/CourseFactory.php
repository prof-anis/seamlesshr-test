<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Course;
use Faker\Generator as Faker;

$factory->define(Course::class, function (Faker $faker) {
    return [
        'text'=>$faker->realText(20),
        'name'=>$faker->realText(10),
        'author'=>$faker->name,
        'status'=>'published'
        
    ];
});
