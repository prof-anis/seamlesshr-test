<?php

namespace App\Http\Controllers\Api;

use Auth;
use Excel;
use App\Course;
use App\Exports\CoursesExport;
use App\Http\Controllers\Controller;
use App\Jobs\Course\CreateCourses;
 use Illuminate\Http\Request;

class CourseController extends Controller
{
	/**
	 * show list of all courses indicating user enrolled courses
	 * @return Response
	 */
	public function index()
	{
		$user = Auth::user();
		$pivot_records = [];
		$user_courses = $user->courses;
		$courses = Course::get();

		foreach ($user_courses as $key => $course) {
			$pivot_records[$course->id] = $course->pivot->created_at;
		}

		foreach ($courses as $key => $course) {
			$course['date_enrolled'] = (in_array($course->id, array_keys($pivot_records))) ? $pivot_records[$course->id] : null;
		}

		 return  $this->successMessage(['courses'=>$courses],'Course retrieved successfully',200);
	}

	/**
	 * creates courses using the course factory
	 * @return Response
	 */
    public function createWithFactory()
    {
    	$number_of_courses = 50;

    	dispatch(new CreateCourses($number_of_courses));

    	return $this->successMessage([],'Course upload successful',202);
    }

   /**
   * Export all courses into a courses.xlsx file.
   * @return File
   */
  public function export()
  {
     
      return Excel::download(new CoursesExport(), 'courses.xlsx');
     
  }
 

}
