<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\User;
use Hash;
use Auth;

class AuthController extends Controller
{
	/**
	 * logs in an exising user into the application
	 * @param  Request $request
	 * @return 
	 */
    public function login(Request $request)
    {
      if (!$this->validateLogin($request)) {
          return $this->failureMessage($this->validationError,'failed to login',422);
      }
      $credentials = $request->only(['email', 'password']);
     
      if (!$token = auth()->attempt($credentials)) {
        return $this->failureMessage(['error'=>'invalid details provided'],'failed to login',401);
      }
      $user = Auth::user();

      return $this->successMessage($this->tokenResponse($token,$user),'Login Successful',200);
    }


    /**
     * registers a new user 
     * @param  Request $request 
     * @return            
     */
    public function register(Request $request)
    {	
    	
        if (!$this->validateRegistration($request)) {
              
              return $this->failureMessage($this->validationError,'registration failed',422);
        }

    		 $user = User::create([
		        'name' => $request->name,
		        'email' => $request->email,
		        'password' => Hash::make($request->password),
		      ]);


		    $token = auth()->login($user);

      	return $this->successMessage($this->tokenResponse($token,$user),'Registration Successful',200);
    	 

		    
    }



    /**
     * prepares the rsponse for the register endpoint
     * @param  string $token 
     * @return Response
     */
    protected function tokenResponse($token,$user)
    {
      return [
        'access_token' => $token,
        'token_type' => 'bearer',
        'expires_in' => auth()->factory()->getTTL() * 60,
        'user'=>$user
      ];
    }

    /**
     * validate the registration data provided
     * @param  Request $request [description]
     * @return bool
     */
    protected function  validateRegistration(Request $request):bool
    {
    	 $validator = Validator::make($request->all(), [
    		'name'=>['required'],
    		'email'=>['required','unique:users,email'],
    		'password'=>['required']
    	]);

      if ($validator->fails()) {
        $this->validationError = $validator->errors()->toJson();
        return false;
       
      }
      return true;

    	
    }

    /**
     * validate the login data provided
     * @param  Request $request 
     * @return bool
     */
    public function validateLogin(Request $request):bool
    {
       $validator = Validator::make($request->all(), [
        'email'=>['required'],
        'password'=>['required']
      ]);

      if ($validator->fails()) {
        $this->validationError = $validator->errors()->toJson();
        return false;
       
      }
      return true;

    }

 
}
