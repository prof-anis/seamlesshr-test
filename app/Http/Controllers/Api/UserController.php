<?php

namespace App\Http\Controllers\Api;

use Auth;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function courseRegistration(Request $request)
    {	
    	if (!$this->courseRegistrationValidation($request)) {
    		
    		return $this->failureMessage($this->validationError,'validation error',422);
    	}

    	$courses = $request->courses;
    	$user = Auth::user();
    	$user->courses()->syncWithoutDetaching($courses);
    	$registeredCourses = $user->courses()->whereIn('courses.id',$courses)->get();
    	
    	return $this->successMessage($registeredCourses,'course registration successful',201);
    }

    protected function courseRegistrationValidation(Request $request)
    {
     $validator = Validator::make($request->all(), [
        'courses' => 'required|array|min:1',
      ]);

      if ($validator->fails()) {
      	$this->validationError = $validator->errors()->toJson();
      	return false;
       
      }
      return true;
    }
}
