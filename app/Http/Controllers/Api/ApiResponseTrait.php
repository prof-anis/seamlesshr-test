<?php

namespace App\Http\Controllers\Api;

trait ApiResponseTrait
{	
	/**
	 * returns a success reponse
	 * @param   array $data        
	 * @param  string $message 
	 * @param int $statusCode     
	 * @return Response              
	 */
	public function successMessage($data,$message,$statusCode)
	{
		return response()->json([
			'data'=>$data,
			'status'=>'success',
			'message'=>$message
		],$statusCode);
	}

	/**
	 * returns a success reponse
	 * @param   array $data        
	 * @param  string $message 
	 * @param int $statusCode      
	 * @return Response              
	 */
	public function failureMessage($data,$message,$statusCode)
	{
		return response()->json([
			'data'=>$data,
			'status'=>'failed',
			'message'=>$message
		],$statusCode);
	}
}