<?php

namespace App\Http\Middleware;

use App\Http\Controllers\Api\ApiResponseTrait;
use Closure;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class JwtAuthMiddleware
{
    use ApiResponseTrait;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
  {
    try {
      $user = JWTAuth::parseToken()->authenticate();
    } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
        return $this->failureMessage([],'Authorization token has Expired', 401);
    }
    catch(\Tymon\JWTAuth\Exceptions\TokenInvalidException $e){
         return $this->failureMessage([],'invalid authorization token', 401);
    }
    catch(\Tymon\JWTAuth\Exceptions\JWTException $e){
        return $this->failureMessage([],'authorization token not found', 401);
    }


 
      
       return $next($request);
  }


}

