<?php

namespace App\Jobs\Course;

use App\Course;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CreateCourses implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $number_of_courses;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($number_of_courses)
    {
        $this->number_of_courses = $number_of_courses;
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        factory(Course::class,$this->number_of_courses)->create();
    }
}
