<?php

namespace Tests;

use App\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    /**
     * returns a dummy model for a model
     */
    
	protected function login($withUser=false)
    {
        //create user
        
        $user = factory(User::class,1)->create()->first();
        
        //login
        $loginData = ['email'=>$user->email,'password'=>'password'];
        $loginResponse =  $this->postJson('/api/v1/auth/login', $loginData);
        
        if($withUser){
        	return [$user,$this->retrieveToken($loginResponse)];
        }
        return $this->retrieveToken($loginResponse);
    }

    public function retrieveToken($login_response)
    {
        return json_decode($login_response->getContent(),true)['data']['access_token'];
    }
}
