<?php

namespace Tests\Feature\Http\Controllers\Api\Auth;


use App\User;
use Faker\Factory as Faker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AuthControllerTest extends TestCase
{   
    use RefreshDatabase;
     

    /**
     * A basic feature test example.
     * @test
     * @return void
     */
    public function can_register_new_user()
    {   
        $faker =  Faker::create();
       
        $user =  [
            'name'=>$faker->name,
            'email'=>$faker->safeEmail,//$faker->safeEmail,
            'password'=>'password'
        ];
        

        $response = $this->postJson('/api/v1/auth/register', $user);

        $response->assertStatus(200)
                ->assertJsonStructure(['status','data'=>['access_token','expires_in','token_type','user'],'message']);

        $this->assertDatabaseHas('users',[
            'name'=>$user['name'],
            'email'=>$user['email']
        ]);
               
               
    }

    public function test_will_throw_422_when_register_validation_fails()
    {
        $user = [];

        $response = $this->postJson('/api/v1/auth/register', $user);

        $response->assertStatus(422)
                ->assertJsonStructure(['status','data','message']);
    }

    /**
     * will_throw_422_when_validation_fails description
     * @test
     * @return [type] [description]
     */
    public function will_throw_422_when_login_validation_fails()
    {
        
    
        $user = [];

        $response = $this->postJson('/api/v1/auth/login', $user);
       
        $response->assertStatus(422)
                ->assertJsonStructure(['status','data','message']);
    }

    /**
     * [test_will_throw_401_when_invalid_data_provided description]
     * @test 
     * @return void
     */
    public function test_will_throw_401_when_invalid_data_provided()
    {
        
    
        $user = ['email'=>'nomail','password'=>'nopass'];

        $response = $this->postJson('/api/v1/auth/login', $user);
       
        $response->assertStatus(401)
                ->assertJsonStructure(['status','data','message']);
    }

    /**
     * [can_login_existing_user description]
     * @test
     * @return [type] [description]
     */
    public function can_login_existing_user()
    {
        $user = factory(User::class,1)->create()->first();
    
        $user = ['email'=>$user->email,'password'=>'password'];

        $response = $this->postJson('/api/v1/auth/login', $user);
       
        $response->assertStatus(200)
                ->assertJsonStructure(['status','data'=>['access_token','expires_in','token_type','user'],'message']);
              

    }

 
}
