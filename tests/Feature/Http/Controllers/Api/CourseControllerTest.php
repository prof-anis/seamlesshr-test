<?php

namespace Tests\Feature\Http\Controllers\Api;

use App\Course;
use App\Jobs\Course\CreateCourses;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Queue;
use Tests\TestCase;

class CourseControllerTest extends TestCase
{   
      use RefreshDatabase;
    
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testWillCreateCoursesWithFactory()
    {
        Queue::fake();
        //login
        $token = $this->login();
        $header = ['Authorization'=>'Bearer '.$token];
        $response = $this->withHeaders($header)->get('/api/v1/course/factory/create');

        $response->assertStatus(202)
                ->assertJsonStructure(['status','data'=>[],'message']);
        Queue::assertPushed(CreateCourses::class, 1);

    }

    public function testWillReturnCourseIndex()
    {
        //create courses 
        $courses = factory(Course::class,3)->create()->pluck('id')->toArray();
        
        //login and get authorization token
        $token = $this->login();
     
        //create courses for user
        $header = ['Authorization'=>'Bearer '.$token];
        $response = $this->withHeaders($header)->postJson('/api/v1/user/course/register',['courses'=>$courses]);
    

        $response = $this->withHeaders($header)->get('/api/v1/course/index');
        
        $response->assertStatus(200);
    }
}
