<?php

namespace Tests\Feature\Http\Controllers\Api;

use App\Course;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserControllerTest extends TestCase
{   
      use RefreshDatabase;
    
    public function testWillRegisterCourseForUser()
    {   
        //create courses 
        $courses = factory(Course::class,3)->create()->pluck('id')->toArray();
        
        //login and get authorization token
        [$user , $token] = $this->login(true);
      
        //send actual request with authorization token
        $header = ['Authorization'=>'Bearer '.$token];
        $response = $this->withHeaders($header)->postJson('/api/v1/user/course/register',['courses'=>$courses]);
        
        //make assertions
        $response->assertStatus(201);
        $this->assertDatabaseHas('course_user',[
            'user_id'=> $user->id,
            'course_id'=>$courses[0]
        ]);
        $this->assertDatabaseHas('course_user',[
            'user_id'=> $user->id,
            'course_id'=>$courses[1]
        ]);
        $this->assertDatabaseHas('course_user',[
            'user_id'=> $user->id,
            'course_id'=>$courses[2]
        ]);
    }

    

    public function testWillReturn422ErrorIfCourseIsNotGivenForCourseRegistration()
    {
         
         
        $token = $this->login();
      
        //send actual request with authorization token
        $header = ['Authorization'=>'Bearer '.$token];
        $response = $this->withHeaders($header)->postJson('/api/v1/user/course/register',['courses'=>[]]);
        
        $response->assertStatus(422);
    }

   

    
}
