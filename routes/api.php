<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware'=>'api'],function(){

	/**
	 * authentication routes
	 */
	Route::group(['prefix'=>'/auth'],function(){

		Route::post('/register', 'Auth\AuthController@register');
		Route::post('/login', 'Auth\AuthController@login');

	});

	/**
	 * courses route
	 */
 
	Route::group(['prefix'=>'/course'],function(){
		Route::get('/factory/create','CourseController@createWithFactory');
		Route::get('/index','CourseController@index');
		Route::get('/export','CourseController@export');
	});
 

	/**
	 * users routes
	 */
	 Route::group(['prefix'=>'/user','middleware'=>'jwt_auth'],function(){
	 	Route::post('/course/register','UserController@courseRegistration');
		Route::get('/course','UserCourseController@index');

	 });
	 
 


});

 